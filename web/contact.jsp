<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Gestion de Librairie | Page d'inscription</title>
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/styles.css" />
<script src="js/jquery-1.12.4-jquery.min.js" type="text/javascript"></script>
<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        
<!--valiadtion css & js start here-->	
<style type="text/css">
label.error
{
	color:red;
	font-family:Times New Roman;
	font-size:17px;
}		
</style>

<script src="js/validate.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
   $('#registerForm').validate({
        rules:
            {
                txt_firstname:
                {
                    required:true
                },
                txt_lastname:
                {
                    required:true
                },
                txt_email:
                {
                    required:true,
                    email:true
                },
                txt_password:
                {
                    required:true,
                    minlength:6,
                    maxlength:12
                },
                txt_role:
                {
                    required:true				
                }
            },
        messages:
            {
                txt_firstname:
                {
                    required:"enter firstname" 
                },
                txt_lastname:
                {
                    required:"enter lastname" 
                },
                txt_email:
                {
                    required:"enter proper email address"  
                },
                txt_password:
                {
                    required:"enter password 6 to 12"
                },
                txt_role:
                {
                    required:"please select role"
                }
            }
   }); 
});
</script>
<!--valiadtion css & js end here-->

</head>

<body>
<%@ include file="intro.html"%>
    
		<center><h2>Veuillez contacter l'Administrateur pour vos crédentiels</h2>
		<p>MOUTOH Khufu  | <span class="text-info">Software Developer</span></p>
        <p>Telephone: (+224)664 222 544 | 622858591</p>
		<p>E-mail : jeanclaver@moutoh.org</p>
		
		 <p> Vous avez un compte connectez-vous ici? <a href="index.jsp">|<span class="text-info"> Compte de connexion </span></a></p>
		</center>
		<form method="post" id="registerForm" class="form-horizontal">
		
		</form>
		</div>
	</div>
	</div>
    <jsp:include page="footer.jsp"></jsp:include>
</body>
</html>

