<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="net.clayan.beans.Livre"%>
<%@page import="net.clayan.beans.Auteur"%>

<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
	<%
			String category = request.getParameter("category");
		if(category != null){
		%>
	<div>
		<span class="label" style="margin-left: 15px;"> Liste des <%= category%>
			Livres
		</span>
	</div>

	<%} %>
	<table id="grid">
		<thead>
			<tr>
				<th id="th-title">Titre du Livre</th>
				<th id="th-author">Auteur</th>
                                <th id="th-prix">Prix</th>
			</tr>
		</thead>


		<tbody>
			<%
        List<Livre> books = (List<Livre>)request.getAttribute("bookList");
        Iterator<Livre> iterator = books.iterator();

        while (iterator.hasNext()) {
          Livre book = (Livre)iterator.next();
          Long Id = book.getId();
          List<Auteur> auteurs = book.getAuteurs();
         
        
  %>
			<tr>
				<th scope="row" id="r100"><%=book.getLivre_titre()%></th>
				<% for (Auteur auteur: auteurs){
						 if ( book.getId().equals(auteur.getLivre_id())){

						  %><td><%=auteur.getNom()+"  " +auteur.getPrenoms()%></td>

				<% }}%>
                                <th scope="row" id="r100"><%=book.getPrix()%></th>
			</tr>

			<%
          }
        
	
  %>

		</tbody>

	</table>
  
</body>
</html>