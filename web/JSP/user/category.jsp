<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="net.clayan.beans.Livre"%>
<%@page import="net.clayan.beans.Auteur"%>

<!DOCTYPE html >
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <script src="js/jquery-1.9.1.js"></script>
        <link rel="stylesheet" href="css/bookstore.css" type="text/css" />
        <script src="js/bookstore.js"></script>


    </head>
    <body>
        <div id="centered">
            <jsp:include page="header.jsp" flush="true" />
            <br />
            <jsp:include page="leftColumn.jsp" flush="true" />
            <%
                    String category = request.getParameter("category");
                    String categoryId = request.getParameter("categoryId");
                    if (category != null) {
            %>
            <div>
                <span class="label" style="margin-left: 15px;">List of <%=category%>Books
                </span>
            </div>

            <%
                    }
            %>
            <table id="grid">
                <thead>
                    <tr>
                        <th id="th-title"> Titre du Livre</th>
                        <th id="th-author">Auteur</th>
                        <th id="th-prix">Prix</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                            List<Livre> livre = (List<Livre>) request.getAttribute("bookList");
                            Iterator<Livre> iterator = livre.iterator();
                            while (iterator.hasNext()) {
                                    Livre book = (Livre) iterator.next();
                                    if (book.getCategorie_id().toString().equals(categoryId)) {
                                            Long Id = book.getId();
                                            List<Auteur> auteurs = book.getAuteurs();
                    %>
                    <tr>
                        <th scope="row" id="r100"><%=book.getLivre_titre()%></th>
                            <%
                                    for (Auteur auteur : auteurs) {
                                                            if (book.getId().equals(auteur.getLivre_id())) {
                            %><td><%=auteur.getNom()+ "  "
									+ auteur.getPrenoms()%></td>

                        <%
                                }
                                                }
                        %>
<th scope="row" id="r100"><%=book.getPrix()%></th>
                    </tr>

                    <%
                            }
                            }
                    %>
                </tbody>

            </table>
                <jsp:include page="footer.jsp" flush="true" />
        </div>
      
            
        
    </body>
</html>