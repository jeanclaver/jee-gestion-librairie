<%-- 
Accessing the Model from the View -Acc�der au mod�le depuis la vue
Le contr�leur transmet � la vue home.jsp � l'aide de RequestDispatcher.
Cette page  leftColumn.jsp utilise le mod�le Category pour afficher les cat�gories dans le menu de gauche de la page d'accueil.

--%>

<%@page language="java" contentType="text/html"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="net.clayan.beans.Livre"%>
<%@page import="net.clayan.beans.Categorie"%>
<%
        String param1 = application.getInitParameter("param1");
%>

<link rel="stylesheet" href="css/bookstore.css" type="text/css" />
<script src="js/bookstore.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
</head>
<div class="leftbar">
    <ul id="menu">
        <li><div>
                <a class="link1" href="<%=param1%>"> <span class="label"
                                                           style="margin-left: 15px;">Accueil</span>
                </a>
            </div></li>
        <li><div>
                <%-- Cette ligne permet d'afficher tous les livres affich�s dans le menu. Lorsque ce lien est cliqu�, la valeur de action-allBooks est ajout�e � l'URL en tant que param�tre, comme indiqu� dans l'URL: http:localhost:8080/JavaWebDevelopment-bookstore/books?action=allBooks --%>
                <a class="link1" href="<%=param1%>?action=allBooks"><span
                        style="margin-left: 15px;" class="label">Tous les livres</span></a>
            </div></li>
        <li><div>
                <span class="label" style="margin-left: 15px;">Categories</span>
            </div>
            <ul>
                <%-- Ici, la liste des cat�gories est obtenue � partir du ServletContext. Nous avions enregistr� la liste des cat�gories dans le ServletContext obtenu � partir de la base de donn�es
                Les d�tails de la cat�gorie sont affich�s dans le balisage, comme la description de la cat�gorie que vous voyez sur la page d'accueil.
                --%>
                <%
                        List<Categorie> categoryList1 = (List<Categorie>) application
                                        .getAttribute("categoryList");
                        Iterator<Categorie> iterator1 = categoryList1.iterator();
                        while (iterator1.hasNext()) {
                                Categorie category1 = (Categorie) iterator1.next();
                %>
                <li><a class="label"
                       href="<%=param1%>?action=category&categoryId=<%=category1.getId()%>&category=<%=category1.getCategorie_description()%>"><span
                            class="label" style="margin-left: 30px;"><%=category1.getCategorie_description()%></span></a>
                </li>
                <%
                        }
                %>
            </ul></li>
        <li><div>
                <span class="label" style="margin-left: 15px;">Contact</span>

            </div></li>
    </ul>
    <form class="search">
        <input type="hidden" name="action" value="search" /> <input id="text"
                                                                    type="text" name="keyWord" size="12" /> <span
                                                                    class="tooltip_message">?</span>
        <p />
        <input id="submit" type="submit" value="Search" />
    </form>


</div>






