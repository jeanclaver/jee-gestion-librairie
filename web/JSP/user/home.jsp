<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="net.clayan.beans.Livre"%>
<!DOCTYPE html >
<%
String imageURL=application.getInitParameter("imageURL"); 

%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="css/bookstore.css" type="text/css" />
        <script type="text/javascript" src="js/jquery-1.9.1.js"></script>
        <script src="js/bookstore.js"></script>
        <title> Bienvenue dans notre Librairie | Page Visiteur </title>
    </head>
    <body>
        <div id="centered">

            <jsp:include page="header.jsp" flush="true" />
            <br />
            <jsp:include page="leftColumn.jsp" flush="true" />
            <span class="label">Livres en vedette</span>
            <table>
                <tr>
                    <td><span class="tooltip_img1"><img
                                src="<%=imageURL%>/book1.png" /></span></td>
                    <td><img src="<%=imageURL%>/book2.png" /></td>
                    <td><img src="<%=imageURL%>/book3.png" /></td>
                    <td><img src="<%=imageURL%>/book4.png" /></td>
                    <td><img src="<%=imageURL%>/book5.png" /></td>
                </tr>
                
                <tr>
                    <td><img src="<%=imageURL%>/book6.png" /></td>
                    <td><img src="<%=imageURL%>/book7.png" /></td>
                    <td><img src="<%=imageURL%>/book8.png" /></td>
                    <td><img src="<%=imageURL%>/book9.png" /></td>
                    <td><img src="<%=imageURL%>/book10.png" /></td>
                </tr>
                <tr>
                    <td><img src="<%=imageURL%>/book11.png" /></td>
                    <td><img src="<%=imageURL%>/book12.png" /></td>
                    <td><img src="<%=imageURL%>/book13.png" /></td>
                    <td><img src="<%=imageURL%>/book16.png" /></td>
                    <td><img src="<%=imageURL%>/book15.png" /></td>
                </tr>
                <tr>
                    <td><img src="<%=imageURL%>/book1.png" /></td>
                    <td><img src="<%=imageURL%>/book18.png" /></td>
                    <td><img src="<%=imageURL%>/book17.png" /></td>
                    <td><img src="<%=imageURL%>/book19.png" /></td>
                    <td><img src="<%=imageURL%>/book20.png" /></td>
                </tr>
            </table>
               <jsp:include page="footer.jsp" flush="true" />
        </div>
        
        
            
        
    </body>
</html>