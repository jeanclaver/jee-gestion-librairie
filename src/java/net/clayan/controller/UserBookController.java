package net.clayan.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.clayan.beans.Livre;
import net.clayan.beans.Categorie;
import net.clayan.dao.BookDAO;
import net.clayan.dao.BookDAOImpl;

@WebServlet(urlPatterns = {"/books"}, description = "Page d'acceueil d'utilisateur")
public class UserBookController extends HttpServlet {

    public UserBookController() {
        super();
    }

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        BookDAO bookDao = new BookDAOImpl();
        // calling DAO method to retrieve bookList from Database
        List<Categorie> categoryList = bookDao.findAllCategories();
        ServletContext context = config.getServletContext();
        context.setAttribute("categoryList", categoryList);

    }
// Line 31: Cette liste de catégories est obtenue à partir de la base de données en appelant findAllCategories() sur l'objet bookDao.
// Line33: La liste des catégories est définie dans le ServletContext afin que la liste soit disponible pour l'ensemble de l'application Web.

    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

// Dispatching to the View - Envoi à la vue
//La méthode doPost() est appelée depuis la méthode doGet().
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        String base = "/JSP/user/";
       
// Cette ligne 49 construit l'URL qui pointe vers la vue de la page d'accueil (home.jsp).
        String url = base + "home.jsp"; 
//Ligne 51 : cette ligne récupère le paramètre d'action de la requête. Mais comme il s'agit d'une page d'accueil, il n'y a pas de paramètre d'action associé, donc la variable action est nulle.
        String action = request.getParameter("action");
        String category = request.getParameter("category");
        String keyWord = request.getParameter("keyWord");
//Lignes 55 à 71 : Le bloc de code des lignes 55 à 71 est ignoré car l'action est nulle. Si l'action n'était pas nulle, l'URL aurait été reconstruite pour pointer vers une vue différente selon que la valeur de l'action était allBooks ou category ou search.
        if (action != null) {
            switch (action) {
                case "allBooks":
                    findAllBooks(request, response);
                    url = base + "listOfBooks.jsp";
                    break;
                case "category":
                    findAllBooks(request, response);
                    url = base + "category.jsp?category=" + category;
                    break;
                case "search":
                    searchBooks(request, response, keyWord);
                    url = base + "searchResult.jsp";
                    break;

            }
        }
//Ligne 73 : Le RequestDispatcher transmet le nom de la vue dans l'URL à la ligne 71.       
        RequestDispatcher requestDispatcher = getServletContext()
                .getRequestDispatcher(url);
        requestDispatcher.forward(request, response);
    }

    private void findAllBooks(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        try {
            BookDAO bookDao = new BookDAOImpl();
            List<Livre> bookList = bookDao.findAllBooks();
            request.setAttribute("bookList", bookList);

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private void searchBooks(HttpServletRequest request,
            HttpServletResponse response, String keyWord)
            throws ServletException, IOException {
        try {
            BookDAO bookDao = new BookDAOImpl();
            List<Livre> bookList = bookDao.searchBooksByKeyword(keyWord);

            request.setAttribute("bookList", bookList);

        } catch (Exception e) {
            System.out.println(e);
        }
    }

}
