package net.clayan.controller;
import java.io.IOException;
 
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.clayan.beans.Membre;
import net.clayan.dao.BookDAO;
import net.clayan.dao.BookDAOImpl;
 
public class LoginServlet extends HttpServlet {

 
public LoginServlet() {
}
 
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
{
    String email = request.getParameter("txt_email");
    String secret  = request.getParameter("txt_password");
    String role_membre  = request.getParameter("txt_role");
System.out.println("Inside servlet");    
    Membre membre = new Membre();
 
    membre.setEmail(email);
    membre.setSecret(secret);
    membre.setRole_membre(role_membre);
    BookDAO bookDao = new BookDAOImpl();
 
    try
    {
        String userValidate = bookDao.authenticateUser(membre);
 
        if(userValidate.equals("Admin_Role"))
        {
            System.out.println("Admin's Home");
 
            HttpSession session = request.getSession(); //Creating a session
            session.setAttribute("Admin", email); //setting session attribute
            request.setAttribute("userName", email);
            
 
            request.getRequestDispatcher("JSP/admin/Admin.jsp").forward(request, response);
        }
        else if(userValidate.equals("Editor_Role"))
        {
            System.out.println("Editor's Home");
 
            HttpSession session = request.getSession();
            session.setAttribute("Editor", email);
            request.setAttribute("userName", email);
 
            request.getRequestDispatcher("JSP/editeur/Editor.jsp").forward(request, response);
        }
        else if(userValidate.equals("User_Role"))
        {
            System.out.println("User's Home");
 
            HttpSession session = request.getSession();
            session.setMaxInactiveInterval(10*60);
            session.setAttribute("User", email);
            request.setAttribute("userName", email);
 
            request.getRequestDispatcher("JSP/user/home.jsp").forward(request, response);
        }
        else
        {
            System.out.println("Error message = "+userValidate);
            request.setAttribute("errMessage", userValidate);
 
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }
    }
    catch (IOException e1)
    {
        e1.printStackTrace();
    }
    catch (Exception e2)
    {
        e2.printStackTrace();
    }
} //End of doPost()
}