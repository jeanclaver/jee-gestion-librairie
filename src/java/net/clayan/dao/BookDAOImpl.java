package net.clayan.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import net.clayan.beans.Auteur;
import net.clayan.beans.Livre;
import net.clayan.beans.Categorie;
import net.clayan.beans.Membre;

public class BookDAOImpl implements BookDAO {

    static {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException ex) {
        }
    }

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:postgresql://192.168.56.10:5432/librairiedb",
                "postgres", "postgres");
    }

    private void closeConnection(Connection connection) {
        if (connection == null) {
            return;
        }
        try {
            connection.close();
        } catch (SQLException ex) {
        }
    }

    @Override
    public String authenticateUser(Membre membre) {
        String email = membre.getEmail();
        String secret = membre.getSecret();
        String role_membre = membre.getRole_membre();
        String sql = "select * from membre where email =? and secret=? and role_membre =?  ";
        Connection connection = null;
        PreparedStatement pstmt = null; //create statement
        try {
            connection = getConnection();
            String emailDB = "";
            String secretDB = "";
            String roleDB = "";
            pstmt = connection.prepareStatement(sql); //sql select query
            pstmt.setString(1, email);
            pstmt.setString(2, secret);    //set above all variable
            pstmt.setString(3, role_membre);
            ResultSet rs = pstmt.executeQuery(); //execute query and set in ResultSet object "rs".

            while (rs.next()) {
                emailDB = rs.getString("email");
                secretDB = rs.getString("secret");  //fetch PostgresSQL database record and store new variable  dbemail,dbpassword,dbrole
                roleDB = rs.getString("role_membre");

                if (email.equals(emailDB) && secret.equals(secretDB) && roleDB.equals("Admin")) {
                    return "Admin_Role";
                } else if (email.equals(emailDB) && secret.equals(secretDB) && roleDB.equals("Editeur")) {
                    return "Editor_Role";
                } else if (email.equals(emailDB) && secret.equals(secretDB) && roleDB.equals("Utilisateur")) {
                    return "User_Role";
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "Invalid user credentials";
    }

    @Override

    public Membre findUser(String email) {
        Membre resp = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            String sqlStr = "Select a.email, a.secret, a.Gender from membre a "//
                    + " where a.email = ? ";
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            if (rs.next()) {

                String secret = rs.getString("secret");
                String gender = rs.getString("genre");
                resp = new Membre();
                
                resp.setEmail(email);
                resp.setSecret(secret);
                resp.setGenre(gender);

            }

        } catch (SQLException ex) {
           ex.printStackTrace();
        } finally {
            try {
                rs.close();
            } catch (Exception ex) {
            }
            try {
                stmt.close();
            } catch (Exception ex) {
            }
            try {
                conn.close();
            } catch (Exception ex) {
            }

        }
        return resp;
    }

    public List<Livre> findAllBooks() {
        List<Livre> result = new ArrayList<Livre>();
        List<Auteur> auteurList = new ArrayList<Auteur>();

        String sql = "select * from livre inner join auteur on livre.id = auteur.livre_id";

        Connection connection = null;
        try {
            connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Livre livre = new Livre();
                Auteur auteur = new Auteur();
                livre.setId(resultSet.getLong("id"));
                livre.setLivre_titre(resultSet.getString("livre_titre"));
                livre.setCategorie_id(resultSet.getLong("categorie_id"));
                livre.setPrix(resultSet.getDouble("prix"));
                auteur.setLivre_id(resultSet.getLong("livre_id"));
                auteur.setNom(resultSet.getString("nom"));
                auteur.setPrenoms(resultSet.getString("prenoms"));
                auteurList.add(auteur);
                livre.setAuteurs(auteurList);
                result.add(livre);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            closeConnection(connection);
        }
        return result;
    }

    @Override
    public List<Livre> searchBooksByKeyword(String keyWord) {
        List<Livre> result = new ArrayList<Livre>();
        List<Auteur> auteurList = new ArrayList<Auteur>();

        String sql = "select * from livre inner join auteur on livre.id = auteur.livre_id"
                + " where livre_titre like '%"
                + keyWord.trim()
                + "%'"
                + " or nom like '%"
                + keyWord.trim()
                + "%'"
                + " or prenoms like '%" + keyWord.trim() + "%'";

        Connection connection = null;
        try {

            connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Livre livre = new Livre();
                Auteur auteur = new Auteur();
                livre.setId(resultSet.getLong("id"));
                livre.setLivre_titre(resultSet.getString("livre_titre"));
                livre.setPrix(resultSet.getDouble("prix"));
                auteur.setNom(resultSet.getString("nom"));
                auteur.setPrenoms(resultSet.getString("prenoms"));
                auteur.setLivre_id(resultSet.getLong("livre_id"));
                auteurList.add(auteur);
                livre.setAuteurs(auteurList);
                result.add(livre);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            closeConnection(connection);
        }

        return result;
    }

    public List<Categorie> findAllCategories() {
        List<Categorie> result = new ArrayList<>();
        String sql = "select * from categorie";

        Connection connection = null;
        try {
            connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Categorie category = new Categorie();
                category.setId(resultSet.getLong("id"));
                category.setCategorie_description(resultSet
                        .getString("categorie_description"));
                result.add(category);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            closeConnection(connection);
        }
        return result;
    }

    public void insert(Livre book) {
        Connection connection = null;
        try {
            connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "insert into livre (livre_titre) values (?)",
                    Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, book.getLivre_titre());
            statement.execute();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                book.setId(generatedKeys.getLong(1));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            closeConnection(connection);
        }
    }

    public void update(Livre book) {
    }

    public void delete(Long bookId) {
        Connection connection = null;

        try {
            connection = getConnection();
            PreparedStatement statement = connection
                    .prepareStatement("delete from livre where id=?");
            statement.setLong(1, bookId);
            statement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            closeConnection(connection);
        }
    }

}
