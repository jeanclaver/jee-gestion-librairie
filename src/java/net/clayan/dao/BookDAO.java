package net.clayan.dao;

import java.util.List;
import net.clayan.beans.Membre;
import net.clayan.beans.Livre;
import net.clayan.beans.Categorie;

public interface BookDAO {
    
        public String authenticateUser(Membre membre);
        public Membre findUser(String email);
	public List<Livre> findAllBooks();
	
	public List<Livre> searchBooksByKeyword(String keyWord);
	
	public List<Categorie> findAllCategories();

	public void insert(Livre book);

	public void update(Livre book);

	public void delete(Long bookId);

}
