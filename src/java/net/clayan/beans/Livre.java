package net.clayan.beans;

import java.util.List;

public class Livre {
	private Long id;
        private String isbn;
	private Long categorie_id;
	private String livre_titre;
        private String editeur;
        private double prix;
	private List<Auteur> auteurs;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the isbn
     */
    public String getIsbn() {
        return isbn;
    }

    /**
     * @param isbn the isbn to set
     */
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    /**
     * @return the categorie_id
     */
    public Long getCategorie_id() {
        return categorie_id;
    }

    /**
     * @param categorie_id the categorie_id to set
     */
    public void setCategorie_id(Long categorie_id) {
        this.categorie_id = categorie_id;
    }

    /**
     * @return the livre_titre
     */
    public String getLivre_titre() {
        return livre_titre;
    }

    /**
     * @param livre_titre the livre_titre to set
     */
    public void setLivre_titre(String livre_titre) {
        this.livre_titre = livre_titre;
    }

    /**
     * @return the editeur
     */
    public String getEditeur() {
        return editeur;
    }

    /**
     * @param editeur the editeur to set
     */
    public void setEditeur(String editeur) {
        this.editeur = editeur;
    }

    /**
     * @return the prix
     */
    public double getPrix() {
        return prix;
    }

    /**
     * @param prix the prix to set
     */
    public void setPrix(double prix) {
        this.prix = prix;
    }

    /**
     * @return the auteurs
     */
    public List<Auteur> getAuteurs() {
        return auteurs;
    }

    /**
     * @param auteurs the auteurs to set
     */
    public void setAuteurs(List<Auteur> auteurs) {
        this.auteurs = auteurs;
    }

    @Override
    public String toString() {
        return "Livre{" + "id=" + id + ", isbn=" + isbn + ", categorie_id=" + categorie_id + ", livre_titre=" + livre_titre + ", editeur=" + editeur + ", prix=" + prix + ", auteurs=" + auteurs + '}';
    }

	

}
