package net.clayan.beans;

public class Auteur {
	private Long id;
	private Long livre_id;
	private String nom;
	private String prenoms;
        private String telephone;
        private String email;
        private String ville;
        private String pays;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the livre_id
     */
    public Long getLivre_id() {
        return livre_id;
    }

    /**
     * @param livre_id the livre_id to set
     */
    public void setLivre_id(Long livre_id) {
        this.livre_id = livre_id;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * @return the prenoms
     */
    public String getPrenoms() {
        return prenoms;
    }

    /**
     * @param prenoms the prenoms to set
     */
    public void setPrenoms(String prenoms) {
        this.prenoms = prenoms;
    }

    /**
     * @return the telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * @param telephone the telephone to set
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the ville
     */
    public String getVille() {
        return ville;
    }

    /**
     * @param ville the ville to set
     */
    public void setVille(String ville) {
        this.ville = ville;
    }

    /**
     * @return the pays
     */
    public String getPays() {
        return pays;
    }

    /**
     * @param pays the pays to set
     */
    public void setPays(String pays) {
        this.pays = pays;
    }

    @Override
    public String toString() {
        return "Auteur{" + "id=" + id + ", livre_id=" + livre_id + ", nom=" + nom + ", prenoms=" + prenoms + ", telephone=" + telephone + ", email=" + email + ", ville=" + ville + ", pays=" + pays + '}';
    }

	

}
