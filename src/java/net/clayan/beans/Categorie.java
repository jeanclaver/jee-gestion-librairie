package net.clayan.beans;

public class Categorie {
	private Long id;
	private String categorie_description;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the categorie_description
     */
    public String getCategorie_description() {
        return categorie_description;
    }

    /**
     * @param categorie_description the categorie_description to set
     */
    public void setCategorie_description(String categorie_description) {
        this.categorie_description = categorie_description;
    }

    @Override
    public String toString() {
        return "Categorie{" + "id=" + id + ", categorie_description=" + categorie_description + '}';
    }

	

}
