/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.clayan.Utils.filter;

import java.sql.Connection;
import java.sql.SQLException;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import net.clayan.beans.Membre;
import net.clayan.dao.BookDAO;
import net.clayan.dao.BookDAOImpl;
import net.clayan.Utils.MyUtils;

/**
 *
 * @author vagrant
 */
@WebFilter(filterName = "cookieFilter", urlPatterns = { "/*" })
public class CookieFilter implements Filter {
public CookieFilter() {
	}

	public void init(FilterConfig fConfig) throws ServletException {

	}

	@Override
	public void destroy() {

	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpSession session = req.getSession();

		Membre userInSession = MyUtils.getLoginedUser(session);
		// 
		if (userInSession != null) {
			session.setAttribute("COOKIE_CHECKED", "CHECKED");
			chain.doFilter(request, response);
			return;
		}

		// La Connecxion a été créée dans JDBCFilter.
		Connection conn = MyUtils.getStoredConnection(request);

		// Le drapeau (flag) sert à vérifier Cookie.
		String checked = (String) session.getAttribute("COOKIE_CHECKED");
		if (checked == null && conn != null) {
			String userName = MyUtils.getUserNameInCookie(req);
                        BookDAO bookDao = new BookDAOImpl();
                        Membre user = bookDao.findUser(userName);
                        MyUtils.storeLoginedUser(session, user);
			// Marquez qu'il a vérifié Cookie.
			session.setAttribute("COOKIE_CHECKED", "CHECKED");
		}

		chain.doFilter(request, response);
	}

}
