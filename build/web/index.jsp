<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Java Web Application| Page d'Acc�s  </title>
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/styles.css" />
        <script src="js/jquery-1.12.4-jquery.min.js" type="text/javascript"></script>
        <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

        <!--valiadtion css & js start here-->	
        <style type="text/css">
            label.error
            {
                color:red;
                font-family:Times New Roman;
                font-size:17px;
            }		
        </style>

        <script src="js/validate.js" type="text/javascript"></script>
        <script type="text/javascript">
        $(document).ready(function(){
           $('#loginForm').validate({
                rules:
                    {
                        txt_email:
                        {
                            required:true,
                            email:true
                        },
                        txt_password:
                        {
                            required:true
                        },
                        txt_role:
                        {
                            required:true				
                        }
                    },
                messages:
                    {
                        txt_email:
                        {
                            required:"entrez votre email address"  
                        },
                        txt_password:
                        {
                            required:"entrez password"
                        },
                        txt_role:
                        {
                            required:"Pri�re de choisir un r�le"
                        }
                    }
           }); 
        });
        </script>

        <!--valiadtion css & js end here-->

    </head>
    <body>
        <%@ include file="intro.html"%>

        <div class="wrapper">
            <div class="container">

                <div class="col-lg-12">
                    <%
            if(request.getAttribute("errorMsg")!=null)
            {
                    %>
                    <div class="alert alert-danger">
                        <strong>WRONG! <% out.println(request.getAttribute("errorMsg")); %></strong>
                    </div>
                    <%
                    }          
                    %>
                    <center><h2>Login Page</h2></center>
                    <form method="post" id="loginForm" class="form-horizontal" action="<%=request.getContextPath()%>/LoginServlet">

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-6">
                                <input type="text" name="txt_email" class="form-control" placeholder="entrer email" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Password</label>
                            <div class="col-sm-6">
                                <input type="password" name="txt_password" class="form-control" placeholder="entrer passowrd" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Choisir Type</label>
                            <div class="col-sm-6">
                                <select name="txt_role" class="form-control">
                                    <option value="" selected="selected"> - choisir role - </option>
                                    <option value="Admin">Admin</option>
                                    <option value="Editeur">Editeur</option>
                                    <option value="Utilisateur">Utilisateur</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9 m-t-15">
                                <input type="submit" name="btn_login" class="btn btn-success" value="Login">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9 m-t-15">
                                Pas encore enregistr� ? <a href="contact.jsp"><p class="text-info">Contactez l'Administrateur</p></a>		
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <jsp:include page="footer.jsp"></jsp:include>	
    </body>
</html>
